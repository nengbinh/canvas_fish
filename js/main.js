/*
weixin - print702
*/

var game;

$(document).ready(()=>{

    game = new FishGame()
    startGame()
    
})

// run it
function startGame(){
    game.game_loop()
    window.requestAnimationFrame(startGame)
}

class FishGame{

    constructor(){
        // Game setting
        this.width = 800
        this.height = 600
        this.deltatime = 0          //用于时间间隔的判断
        this.lasttime = Date.now()
        this.mx = this.width * 0.5  //记录鼠标当前的位置
        this.my = this.height * 0.5
        this.WaterMove = 0.0007     //海水移动幅度
        // run html funtion
        this.html_build()
        // 
        this.ctx1 = document.getElementById('canvas1').getContext('2d')
        this.ctx2 = document.getElementById('canvas2').getContext('2d')
        // background image
        this.bg = new Image() //加载背景图片
        this.bg.src = './img/background.jpg'
        // 监控鼠标移动
        $('#canvas1').on("mousemove",this.mouseMove.bind(this));
        // 初始化
        this.ane_init()
        this.fruit_init()
        this.Mfish_init()
        this.Bfish_init()
        this.score_init()
        this.wave_init()
        this.halo_init()
        this.dust_init()
    }

    //鼠标移动函数
    mouseMove(e){
        if(this.gameOver) return  //判断游戏是否结束
        if(e.offsetX || e.layerX){
            this.mx = e.offsetX == undefined ? e.layerX : e.offsetX
            this.my = e.offsetY == undefined ? e.layerY : e.offsetY
        }
    }

    // append html element
    html_build(){
        // HTML
        let div1 = $("<div></div>")
        div1.addClass('all_bg')
        let div2 = $("<div></div>")
        div2.addClass('allcanvas')
        let can1 = $("<canvas></canvas>")
        let can2 = $("<canvas></canvas>")
        can1.attr('id','canvas1')
        can2.attr('id','canvas2')
        can1.attr('width',this.width)
        can1.attr('height',this.height)
        can2.attr('width',this.width)
        can2.attr('height',this.height)
        div2.append(can1)
        div2.append(can2)
        div1.append(div2)
        $('body').append(div1)
    }

    // draw Background
    draw_background(){
        this.ctx2.drawImage(this.bg,0,0,this.width,this.height)
    }
    ////////////////////////////////// Dust ////////////////////////////////////

    //漂浮物
    dust_init(){
        this.dust_x = []    //x坐标
        this.dust_y = []    //y坐标
        this.dust_apm = [] 
        this.dust_NO = []   //使用哪个图片
        this.dust_sin = 0   //正弦
        this.dust_num = 30  //数量

        this.dustpic = []   //图片
        for(let i=0; i<7; i++){
            this.dustpic[i] = new Image()
            this.dustpic[i].src = './img/dust'+i+'.png'
        }

        // 随机
        for(let i=0; i<this.dust_num; i++){
            this.dust_x[i] = Math.random() * this.width
            this.dust_y[i] = Math.random() * this.height
            this.dust_apm[i] = 20 + Math.random() * 15
            this.dust_NO[i] = Math.floor(Math.random() * 7)//[0,7]
        }
    }

    dust_draw(){
        this.dust_sin += this.deltatime * this.WaterMove
        let l = Math.sin(this.dust_sin)
        for(let i=0; i<this.dust_num; i++){
            let no = this.dust_NO[i]
            this.ctx1.drawImage(this.dustpic[no], this.dust_x[i]+this.dust_apm[i] * l, this.dust_y[i])
        }
    }

    ////////////////////////////////// Mom Fish ////////////////////////////////////

    Mfish_init(){
        //
        this.Mfish_x = this.width * 0.5                 //鱼妈妈x值
        this.Mfish_y = this.height * 0.5                //鱼妈妈y值
        this.Mfish_angel = 0                            //鱼妈妈的角度

        //眼睛图片
        this.Mfish_eye_timer = 0 //更换图片时间间隔
        this.Mfish_eye_count = 0 //图片序列
        this.Mfish_eye_interval = 1000 //时间间隔
        this.Mfish_eye = []
        for(let i=0;i<2;i++){
            this.Mfish_eye[i] = new Image()
            this.Mfish_eye[i].src = './img/bigEye'+i+'.png'
        }

        //身体图片
        this.Mfish_body_count = 0 //图片序列
        this.Mfish_obody = []
        this.Mfish_bbody = []
        for(let i=0;i<8;i++){
            this.Mfish_obody[i] = new Image()
            this.Mfish_bbody[i] = new Image()
            this.Mfish_obody[i].src = './img/bigSwim'+i+'.png'
            this.Mfish_bbody[i].src = './img/bigSwimBlue'+i+'.png'
        }

        //尾巴图片
        this.Mfish_tail_timer = 0 //更换图片时间间隔
        this.Mfish_tail_count = 0 //图片序列
        this.Mfish_tail_interval = 50 //时间间隔
        this.Mfish_tail = []
        for(let i=0;i<8;i++){
            this.Mfish_tail[i] = new Image()
            this.Mfish_tail[i].src = './img/bigTail'+i+'.png'
        }

    }

    // 鱼妈妈跟果实的碰撞检测
    Mfish_eat(){
        if(this.gameOver) return  //判断游戏是否结束
        this.fruit_alive.forEach((val,key)=>{
            if(val){
                // 获取距离
                let des = calLength2(this.fruit_x[key],this.fruit_y[key],this.Mfish_x,this.Mfish_y)
                // 吃到了果实
                if(des<900) {
                    // 把果实alive状态设置
                    this.fruit_alive[key]=false
                    console.log('eaten')
                    // 鱼妈妈身体序列变化
                    this.Mfish_body_count ++
                    if(this.Mfish_body_count>7){
                        this.Mfish_body_count=7
                    }
                    // 增加分数
                    this.score_num++
                    if(this.fruit_type[key]=='blue') {
                        this.score_double = 2
                    }
                    // 光圈
                    this.wave_born(this.fruit_x[key],this.fruit_y[key])
                }
            }
        })
    }

    // 鱼妈妈喂小鱼的碰撞检测过程
    Mfish_feed(){
        if(this.gameOver) return   //判断游戏是否结束
        // 如果吃到果实的状态下
        if(this.score_num){
            // 获取距离
            let distance = calLength2(this.Mfish_x,this.Mfish_y,this.Bfish_x,this.Bfish_y)
            // 判断距离
            if(distance<900){            
                this.Bfish_body_count = 0
                this.Mfish_body_count = 0 //恢复大鱼身体序列图片
                // 增加分数
                this.score_add()
                // 光圈
                this.halo_born(this.Bfish_x,this.Bfish_y)
            }
        }
    }

    // 鱼妈妈的绘制过程
    Mfish_draw(){
        /*
        因为此方法只适合鱼妈妈 所以用save跟restore
        然后translate画布起始点到设定好的x跟y值
        */
        //改变x，y值
        this.Mfish_y = lerpDistance(this.my, this.Mfish_y, 0.98)
        this.Mfish_x = lerpDistance(this.mx, this.Mfish_x, 0.98)
        /*
        计算角度差 Math.atan2(y,x)
        */
        let delY = this.my - this.Mfish_y
        let delX = this.mx - this.Mfish_x
        let beta = Math.atan2(delY, delX) + Math.PI // -PI , PI
        // lerp angel
        this.Mfish_angel = lerpAngle(beta, this.Mfish_angel, 0.6)

        // 眼睛图片序列
        this.Mfish_eye_timer += this.deltatime
        if(this.Mfish_eye_timer > this.Mfish_eye_interval){
            this.Mfish_eye_count = (this.Mfish_eye_count + 1) % this.Mfish_eye.length
            this.Mfish_eye_timer %= this.Mfish_eye_interval
            // 如果眼睛当前图片序列是1 就把延迟执行设置为200毫秒
            // 图片序列1代表闭眼睛
            // 眨眼时间设置为2秒以上 加上随机值让其看起来自然
            this.Mfish_eye_count == 1 ?
                this.Mfish_eye_interval = 200 :
                this.Mfish_eye_interval = Math.random() * 1500 + 2000
        }
        // 尾巴图片序列
        this.Mfish_tail_timer += this.deltatime
        if(this.Mfish_tail_timer > this.Mfish_tail_interval){
            this.Mfish_tail_count = (this.Mfish_tail_count + 1) % this.Mfish_tail.length
            this.Mfish_tail_timer %= this.Mfish_tail_interval
        }
        // 要绘制的图片
        let eyeimg = this.Mfish_eye[this.Mfish_eye_count]
        let tailimg = this.Mfish_tail[this.Mfish_tail_count]
        // 判断吃到了什么果实
        if(this.score_double == 2){
            var bodyimg = this.Mfish_bbody[this.Mfish_body_count]
        }else{
            var bodyimg = this.Mfish_obody[this.Mfish_body_count]
        }
        //
        this.ctx1.save()
        this.ctx1.translate(this.Mfish_x, this.Mfish_y)
        this.ctx1.rotate(this.Mfish_angel)
        this.ctx1.drawImage(tailimg, -tailimg.width * 0.5 + 30, -tailimg.height * 0.5)
        this.ctx1.drawImage(bodyimg, -bodyimg.width * 0.5, -bodyimg.height * 0.5)
        this.ctx1.drawImage(eyeimg, -eyeimg.width * 0.5, -eyeimg.height * 0.5)
        this.ctx1.restore()
    }

    ////////////////////////////////// Baby Fish ////////////////////////////////////

    //实现过程跟鱼妈妈一样

    Bfish_init(){
        //
        this.Bfish_x = this.width * 0.5 + 50                 //鱼宝宝x值
        this.Bfish_y = this.height * 0.5 + 50                //鱼宝宝y值
        this.Bfish_angel = 0                            //鱼宝宝的角度
        //鱼宝宝身体图片
        this.Bfish_body_timer = 0 //更换图片时间间隔
        this.Bfish_body_count = 0 //图片序列
        this.Bfish_body_interval = 300 //时间间隔
        this.Bfish_body = []
        for(let i=0;i<20;i++){
            this.Bfish_body[i] = new Image()
            this.Bfish_body[i].src = './img/babyFade'+i+'.png'
        }
        //鱼宝宝眼睛图片
        this.Bfish_eye_timer = 0 //更换图片时间间隔
        this.Bfish_eye_count = 0 //图片序列
        this.Bfish_eye_interval = 1000 //时间间隔
        this.Bfish_eye = []
        for(let i=0;i<2;i++){
            this.Bfish_eye[i] = new Image()
            this.Bfish_eye[i].src = './img/babyEye'+i+'.png'
        }
        //鱼宝宝尾巴图片
        this.Bfish_tail_timer = 0 //更换图片时间间隔
        this.Bfish_tail_count = 0 //图片序列
        this.Bfish_tail_interval = 50 //时间间隔
        this.Bfish_tail = []
        for(let i=0;i<8;i++){
            this.Bfish_tail[i] = new Image()
            this.Bfish_tail[i].src = './img/babyTail'+i+'.png'
        }
    }

    Bfish_draw(){
        //改变x，y值
        this.Bfish_y = lerpDistance(this.Mfish_y + 25, this.Bfish_y, 0.98)
        this.Bfish_x = lerpDistance(this.Mfish_x + 25, this.Bfish_x, 0.98)
        /*
        计算角度差 Math.atan2(y,x)
        */
        let delY = this.Mfish_y - this.Bfish_y
        let delX = this.Mfish_x - this.Bfish_x
        let beta = Math.atan2(delY, delX) + Math.PI // -PI , PI
        // lerp angel
        this.Bfish_angel = lerpAngle(beta, this.Bfish_angel, 0.6)
        // 眼睛图片序列
        this.Bfish_eye_timer += this.deltatime
        if(this.Bfish_eye_timer > this.Bfish_eye_interval){
            this.Bfish_eye_count = (this.Bfish_eye_count + 1) % this.Bfish_eye.length
            this.Bfish_eye_timer %= this.Bfish_eye_interval
            // 如果眼睛当前图片序列是1 就把延迟执行设置为200毫秒
            // 图片序列1代表闭眼睛
            // 眨眼时间设置为2秒以上 加上随机值让其看起来自然
            this.Bfish_eye_count == 1 ?
                this.Bfish_eye_interval = 200 :
                this.Bfish_eye_interval = Math.random() * 1500 + 2000
        }
        // 身体图片序列
        this.Bfish_body_timer += this.deltatime
        if(this.Bfish_body_timer > this.Bfish_body_interval){
            this.Bfish_body_count ++
            this.Bfish_body_timer %= this.Bfish_body_interval
            if(this.Bfish_body_count>this.Bfish_body.length-1){
                this.Bfish_body_count=this.Bfish_body.length-1
                //game over
                this.gameOver = true
            }
        }
        // 尾巴图片序列
        this.Bfish_tail_timer += this.deltatime
        if(this.Bfish_tail_timer > this.Bfish_tail_interval){
            this.Bfish_tail_count = (this.Bfish_tail_count + 1) % this.Bfish_tail.length
            this.Bfish_tail_timer %= this.Bfish_tail_interval
        }
        // 要绘制的图片
        let eyeimg = this.Bfish_eye[this.Bfish_eye_count]
        let tailimg = this.Bfish_tail[this.Bfish_tail_count]
        let bodyimg = this.Bfish_body[this.Bfish_body_count]
        //
        this.ctx1.save()
        this.ctx1.translate(this.Bfish_x, this.Bfish_y)
        this.ctx1.rotate(this.Bfish_angel)
        this.ctx1.drawImage(tailimg, - tailimg.width * 0.5 + 23, -tailimg.height * 0.5)
        this.ctx1.drawImage(bodyimg, -bodyimg.width * 0.5, -bodyimg.height * 0.5)
        this.ctx1.drawImage(eyeimg, -eyeimg.width * 0.5, -eyeimg.height * 0.5)
        this.ctx1.restore()
    }

    ////////////////////////////////////// fruit ////////////////////////////////////

    fruit_init(){
        // fruit setting
        this.fruit_alive = []   //每个果实的状态
        this.fruit_x = []       //每个果实的x值
        this.fruit_y = []       //每个果实的y值
        this.fruit_l = []       //每个果实的大小半径
        this.fruit_aneID = []   //每个果实对于的海葵
        this.fruit_speed = []   //每个果实生长以及漂浮速度
        this.fruit_img = []     //每个果实的图片样式
        this.fruit_type = []    //每个果实的类型
        this.fruit_num = 15     //果实的数量
        this.fruit_orange = new Image()
        this.fruit_orange.src = './img/fruit.png'
        this.fruit_blue = new Image()
        this.fruit_blue.src = './img/blue.png'
        // 把数组都给初始化一下
        for(let i=0; i<this.fruit_num; i++){
            this.fruit_alive[i] = false
            this.fruit_x[i] = 0
            this.fruit_y[i] = 0
            this.fruit_l[i] = 0
        }
    }

    // 果实的出生过程
    fruit_born(i){
        /*
        随机取出个海葵的下标
        果实的X值 是海葵的X值
        果实的Y值 是cavas画布的高度减去海葵的高度
        果实的半径大小设置为0
        */
        this.fruit_aneID[i] = Math.floor(Math.random() * this.ane_num)
        this.fruit_alive[i] = true
        this.fruit_l[i] = 0
        this.fruit_x[i] = 100
        this.fruit_y[i] = 100
        this.fruit_speed[i] = Math.random() * 0.01 + 0.005
        this.fruit_randimg(i)
    }

    // 随机果实样式
    fruit_randimg(i){
        if(Math.floor(Math.random()*2)){
            this.fruit_img[i] = this.fruit_orange
            this.fruit_type[i] = 'orange'
        }else{
            this.fruit_img[i] = this.fruit_blue
            this.fruit_type[i] = 'blue'
        }
    }

    //果实绘制过程
    fruit_draw(){
        /*
        进入循环
            如果当前果实状态为true就执行
                如果果实的半径小于14
                    就改变果实的半径
                如果不是
                    就改变果实的Y值
                绘制果实
        如果果实为false
            重新出生
        如果当前果实的y值小于10
            就把果实的状态设置为false
        */
        for(let i=0; i<this.fruit_num; i++){
            
            if(this.fruit_alive[i]){
                if(this.fruit_l[i]<=14){ //生长中
                    let num = this.fruit_aneID[i]
                    this.fruit_x[i] = this.ane_headx[num]
                    this.fruit_y[i] = this.ane_heady[num]
                    this.fruit_l[i] += this.fruit_speed[i] * this.deltatime
                }else{ //成熟
                    this.fruit_y[i] -= (this.fruit_speed[i] * 7) * this.deltatime 
                }
                this.ctx2.drawImage(this.fruit_img[i], 
                    this.fruit_x[i] - this.fruit_l[i] * 0.5 ,
                    this.fruit_y[i] - this.fruit_l[i] * 0.5 ,
                    this.fruit_l[i], this.fruit_l[i]
                    )
            }
            if(!this.fruit_alive[i]) this.fruit_born(i)
            if(this.fruit_y[i] < 10) this.fruit_alive[i] = false
        }
    }

    ////////////////////////////////////// halo /////////////////////////////////////

    // 大鱼喂小鱼的光圈特效
    halo_init(){
        this.halo_x = []
        this.halo_y = [] 
        this.halo_r = []     // 半径
        this.halo_alive = [] // 判断是否可用
        this.halo_num = 5   // 一共多少数量
        this.halo_maxR = 100  // 圆圈最大的半径
        //
        for(let i=0; i<this.halo_num; i++){
            this.halo_alive[i] = false
        }
    }

    halo_born(x,y){
        this.halo_alive.some((val,key)=>{
            if(!val){
                this.halo_alive[key] = true
                this.halo_r[key] = 10
                this.halo_x[key] = x
                this.halo_y[key] = y
                return true
            }
        })
    }

    halo_draw(){
        this.ctx1.save()
        this.ctx1.lineWidth = 2 //美化过程
        this.ctx1.shadowBlur = 10
        this.ctx1.shadowColor = 'red'
        //
        this.halo_alive.forEach((val,key)=>{
            if(val){
                this.halo_r[key] += this.deltatime * 0.06 // 半价逐渐变大
                if(this.halo_r[key] > this.halo_maxR) this.halo_alive[key]=false // 半价超过100 设置为完成
                let alpha = 1 - this.halo_r[key] / this.halo_maxR //透明度跟半价成反
                //画圆光效
                this.ctx1.beginPath()
                this.ctx1.arc(this.halo_x[key], this.halo_y[key], this.halo_r[key], 0, Math.PI * 2)
                this.ctx1.closePath()
                this.ctx1.strokeStyle = "rgba(255,0,0,"+alpha+")"
                this.ctx1.stroke()
            }
        })
        this.ctx1.restore()
    }

    ////////////////////////////////////// wave /////////////////////////////////////

    // 大鱼吃果实的光圈特效

    wave_init(){
        this.wave_x = []
        this.wave_y = [] 
        this.wave_r = []     // 半径
        this.wave_alive = [] // 判断是否可用
        this.wave_num = 10   // 一共多少数量
        this.wave_maxR = 50  // 圆圈最大的半径
        //
        for(let i=0; i<this.wave_num; i++){
            this.wave_alive[i] = false
        }
    }

    wave_draw(){
        this.ctx1.save()
        this.ctx1.lineWidth = 2 //美化过程
        this.ctx1.shadowBlur = 10
        this.ctx1.shadowColor = 'white'
        //
        this.wave_alive.forEach((val,key)=>{
            if(val){
                this.wave_r[key] += this.deltatime * 0.05 // 半价逐渐变大
                if(this.wave_r[key] > this.wave_maxR) this.wave_alive[key]=false // 半价超过100 设置为完成
                let alpha = 1 - this.wave_r[key] / this.wave_maxR //透明度跟半价成反
                //画圆光效
                this.ctx1.beginPath()
                this.ctx1.arc(this.wave_x[key], this.wave_y[key], this.wave_r[key], 0, Math.PI * 2)
                this.ctx1.closePath()
                this.ctx1.strokeStyle = "rgba(255,255,255,"+alpha+")"
                this.ctx1.stroke()
            }
        })
        this.ctx1.restore()
    }

    wave_born(fx,fy){
        this.wave_alive.some((val,key)=>{
            if(!val){
                this.wave_alive[key] = true
                this.wave_r[key] = 10
                this.wave_x[key] = fx
                this.wave_y[key] = fy
                return true
            }
        })
    }
    ////////////////////////////////////// ane //////////////////////////////////////
    ane_init(){
        // ane setting
        this.ane_num = 50           //海葵数量
        this.ane_rootx = []         //海葵根部点
        this.ane_headx = []         //海葵头部x
        this.ane_heady = []         //海葵头部y
        this.ane_amp = []           //摆动
        this.ane_sin = 0 
        // 可调整 setting
        this.ane_space = 16         //海葵之间的间隔
        this.ane_height = 250       //海葵默认高度
        this.ane_move = 40          //海葵摆动范围
        this.ane_movePos = 100      //海葵摆动高度
        this.ane_width = 20         //画笔宽度
        this.ane_alpha = 0.6        //透明度
        this.ane_color = '#3b154e'  //颜色

        
        //
        for(let i=0; i<this.ane_num; i++){
            this.ane_rootx[i] = i * this.ane_space + Math.random() * 20
            this.ane_headx[i] = this.ane_rootx[i]
            this.ane_heady[i] = this.height - this.ane_height + Math.random() * 50
            this.ane_amp[i] = Math.random() * this.ane_move + this.ane_move
        }
    }

    // draw ane
    draw_ane(){
        this.ane_sin += this.deltatime * this.WaterMove
        let l = Math.sin(this.ane_sin)
        this.ctx2.save()                                // save setting
        this.ctx2.globalAlpha = this.ane_alpha          //画笔透明度
        this.ctx2.lineWidth = this.ane_width            //画笔宽度
        this.ctx2.lineCap = 'round'                     //线条首位样式
        this.ctx2.strokeStyle = this.ane_color          //画笔颜色
        for(let i=0; i< this.ane_num; i++){
            this.ctx2.beginPath()
            this.ctx2.moveTo(this.ane_rootx[i],this.height) //画笔起始点
            this.ane_headx[i] = this.ane_rootx[i] + l * this.ane_amp[i]
            this.ctx2.quadraticCurveTo(this.ane_rootx[i], this.height-this.ane_movePos, this.ane_headx[i], this.ane_heady[i]) //线条终点
            this.ctx2.stroke()                          //开始绘制
        }
        this.ctx2.restore()                             // restore setting back
    }
    ////////////////////////////////// score //////////////////////////////////////

    
    score_init(){
        this.score_num = 0              // 吃掉多少个果实
        this.score_double = 1           // 是否吃到蓝色果实
        this.score_final = 0            // 游戏分数
        this.gameOver = false           // 用于判断是否游戏结束
        this.gameOverAlpha = 0          // 字体透明度
        // 画笔设置
        this.ctx1.font = '30px Verdana' 
        this.ctx1.textAlign = 'center'
    }

    // 清空果实积分
    score_reset(){
        this.score_num = 0
        this.score_double = 1
    }

    //游戏积分
    score_add(){
        this.score_final += this.score_num * 100 * this.score_double
        this.score_reset()
    }

    //绘制到画面上
    score_draw(){
        let w = this.width
        let h = this.height
        this.ctx1.save()
        this.ctx1.shadowBlur = 10     // 阴影范围
        this.ctx1.shadowColor = 'red' // 阴影颜色
        this.ctx1.fillStyle = 'white' // 画笔颜色
        // 绘制出文字
        this.ctx1.fillText('SCORE: '+this.score_final, w * 0.5, h - 20)
        // 判断是否游戏结束了
        if(this.gameOver){
            this.gameOverAlpha += this.deltatime * 0.0005
            if(this.gameOverAlpha>1) this.gameOverAlpha = 1
            this.ctx1.fillStyle = "rgba(255,255,255,"+this.gameOverAlpha+")"
            this.ctx1.fillText('GAME OVER', w * 0.5, h * 0.5)
        }
        this.ctx1.restore()
    }

    ///////////////////////////////////////////////////////////////////////////////

    // game looping __ main
    game_loop(){
        let now = Date.now()
        this.deltatime = now - this.lasttime
        this.lasttime = now
        if(this.deltatime>50) this.deltatime=50
        //
        this.draw_background()
        this.draw_ane()
        this.fruit_draw()
        // 因为canvas1是透明的 所以会出现重复
        // 需要清空一下内容先
        this.ctx1.clearRect(0, 0, this.width, this.height)
        // 要绘制的
        this.Mfish_draw()
        this.Bfish_draw()
        this.Mfish_eat()
        this.Mfish_feed()
        this.score_draw()
        this.wave_draw()
        this.halo_draw()
        this.dust_draw()
    }

    

    

}